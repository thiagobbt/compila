%{
	#include <stdio.h>
    #include <iostream>
	#include "type.h"

	NBlock *programBlock;

	#define YYERROR_VERBOSE 1
	#define YYDEBUG 1
	#define ARRAY_NOT_DECLARED_ERROR(x) "Erro: array \"" + x + "\" não declarado!"
	#define VARIABLE_NOT_DECLARED_ERROR(x) "Erro: variável \"" + x + "\" não declarada!"
	#define FUNCTION_DECLARED_ERROR(x) "Erro: função \"" + x + "\" já foi declarada!"
	//#define DIFFERENT_RETURN_TYPE_ERROR(function_name, type, return_type) "Erro: tipo de retorno \"" << return_type << "\" não é compatível com a função \"" << function_name << "\"" << type << "\""
	// #define DEBUG(x) std::cout << x << std::endl;
	#define DEBUG(x) {};

	int yylex();
	void yyerror(const char *s, ...);

	extern char *yytext;

    bool declared(const std::string &name) {
        return (declared_variables.count(name) > 0);
    }

    bool array_declared(const std::string &name) {
        return (declared_arrays.count(name) > 0);
    }

    bool function_declared(const std::string &name) {
        return (declared_functions.count(name) > 0);
    }
%}

%token INCLUDE

%token <token> AND OR
%token <token> PLUS MINUS TIMES DIVIDE
%token <token> CMP_LT CMP_GT CMP_LE CMP_GE CMP_EQ CMP_NE
%token <token> TRUE_T FALSE_T

%token FUNCTION
%token WHILE
%token IF
%token ELSE
%token RETURN

%token OPEN_PARENTHESIS
%token CLOSE_PARENTHESIS

%token OPEN_CURLY_BRACES
%token CLOSE_CURLY_BRACES

%token OPEN_BRACKET
%token CLOSE_BRACKET

%token SEMICOLON
%token COMMA

%token ASSIGNMENT_OPERATOR
%token <token> BOOLEAN_OPERATOR COMPARISON_OPERATOR

%type <token> comparison-operator

%type  <type> type-specifier
%token <type> VOID CHAR INT DOUBLE STRING BOOL

%token <type_value> NUMBER INTEGER CHAR_LITERAL STRING_LITERAL IDENTIFIER

%type <exprvec> argument-list
%type <varvec> parameter-list

%type <token> logical-operator
%type <ident> ident
%type <return_stmt> return-statement
%type <expr> term factor expression arithmetic-expression comparison-expression literal function-call attribution boolean-expression
%type <block> program stmts else-statement
// %type <stmt> include-statement
%type <stmt> stmt function-declaration if-statement while-statement
%type <var_decl> variable-declaration

%%

program: stmts {
	programBlock = $1;
	// std::cout << "ok" << std::endl;
	// std::cout << *programBlock;
} ;

stmts:	stmt {
			$$ = new NBlock();
			$$->statements.push_back($<stmt>1);
		} | stmts stmt {
			$1->statements.push_back($<stmt>2);
		};

stmt: variable-declaration SEMICOLON {$$ = $1;}
		| function-declaration
		| if-statement
		| while-statement
		| expression {
			$$ = new NExpressionStatement(*$1);
		} ;

expression: attribution SEMICOLON |
			function-call SEMICOLON ;

attribution: ident ASSIGNMENT_OPERATOR term {
            $$ = new NAssignment(*$1, *$3);
        } | ident OPEN_BRACKET INTEGER CLOSE_BRACKET ASSIGNMENT_OPERATOR term {
        	$$ = new NAssignment(*$1, $3.int_val, *$6);
        };


variable-declaration: type-specifier ident {
			$$ = new NVariableDeclaration($1, *$2);
		} | type-specifier ident ASSIGNMENT_OPERATOR term {
			$$ = new NVariableDeclaration($1, *$2, $4);
		} | type-specifier ident OPEN_BRACKET CLOSE_BRACKET ASSIGNMENT_OPERATOR OPEN_CURLY_BRACES argument-list CLOSE_CURLY_BRACES {
			$$ = new NArrayDeclaration($1, *$2, *$7);
		} | type-specifier ident OPEN_BRACKET INTEGER CLOSE_BRACKET {
			$$ = new NArrayDeclaration($1, *$2, $4.int_val);
		};


type-specifier: VOID | CHAR | INT | DOUBLE | STRING | BOOL;

return-statement: RETURN term SEMICOLON {
	$$ = new NReturnStatement(*$2);
} | RETURN SEMICOLON {
	$$ = new NReturnStatement(NVoid());
};

// parameter: type-specifier ident {
// 	$$.type = $1;
// 	$$.name = $2.name;
// };

parameter-list: variable-declaration {
			$$ = new VariableList();
			$$->push_back($1);
		} | parameter-list COMMA variable-declaration {
			$1->push_back($3);
		};

function-declaration: type-specifier ident OPEN_PARENTHESIS
			parameter-list CLOSE_PARENTHESIS OPEN_CURLY_BRACES stmts
			return-statement CLOSE_CURLY_BRACES {
				$$ = new NFunctionDeclaration($1, *$2, *$4, *$7, *$8);
			} | type-specifier ident OPEN_PARENTHESIS CLOSE_PARENTHESIS
			OPEN_CURLY_BRACES stmts return-statement CLOSE_CURLY_BRACES {
				$$ = new NFunctionDeclaration($1, *$2, VariableList(), *$6, *$7);
			};

// include-statement: INCLUDE STRING_LITERAL ;

if-statement: IF OPEN_PARENTHESIS comparison-expression CLOSE_PARENTHESIS OPEN_CURLY_BRACES
				stmts CLOSE_CURLY_BRACES else-statement {
					$$ = new NIfStatement(*$3, *$6, *$8);
				};

else-statement: ELSE if-statement {
	$$ = new NBlock();
	$$->statements.push_back($2);
} | ELSE OPEN_CURLY_BRACES stmts CLOSE_CURLY_BRACES {
	$$ = $3;
} | {
	$$ = new NBlock();
} ;

while-statement: WHILE OPEN_PARENTHESIS comparison-expression CLOSE_PARENTHESIS
				OPEN_CURLY_BRACES stmts CLOSE_CURLY_BRACES {
					$$ = new NWhileStatement(*$3, *$6);
				};

function-call: ident OPEN_PARENTHESIS CLOSE_PARENTHESIS {
					$$ = new NFunctionCall(*$1);
				} | ident OPEN_PARENTHESIS argument-list CLOSE_PARENTHESIS {
					$$ = new NFunctionCall(*$1, *$3);
				};

argument-list: term {
	$$ = new ExpressionList();
	$$->push_back($1);
} | argument-list COMMA term {
	$1->push_back($3);
	// $3->print(std::cout, 0);
};

logical-operator: AND | OR ;

comparison-operator: CMP_LT | CMP_GT | CMP_LE | CMP_GE | CMP_EQ | CMP_NE;

comparison-expression: OPEN_PARENTHESIS comparison-expression CLOSE_PARENTHESIS {
					$$ = $2;
				} | BOOLEAN_OPERATOR comparison-expression {
					$$ = new NUnaryLOperator($1, *$2);
				} | comparison-expression comparison-operator comparison-expression {
					$$ = new NBinaryOperator(*$1, $2, *$3);
				} | comparison-expression logical-operator comparison-expression {
					$$ = new NBinaryOperator(*$1, $2, *$3);
				} | term ;




/* PRODUÇÕES PRONTAS? */

arithmetic-expression: OPEN_PARENTHESIS arithmetic-expression CLOSE_PARENTHESIS {
					$$ = $2;
				} | arithmetic-expression PLUS factor {
					auto wtf = $2;
					$$ = new NBinaryOperator(*$1, $2, *$3);
				} |	arithmetic-expression MINUS factor {
					$$ = new NBinaryOperator(*$1, $2, *$3);
				} | factor;

factor: factor TIMES term { $$ = new NBinaryOperator(*$1, $2, *$3); } |
				factor DIVIDE term { $$ = new NBinaryOperator(*$1, $2, *$3); } |
				term;

boolean-expression: TRUE_T {
	$$ = new NBoolean(true);
} | FALSE_T {
	$$ = new NBoolean(false);
};

term: arithmetic-expression |
        NUMBER {$$ = new NDouble($1.double_val);} |
        INTEGER {$$ = new NInteger($1.int_val);} |
        ident {
        	$$ = $1;
        } |
        ident OPEN_BRACKET INTEGER CLOSE_BRACKET {
        	$1->index = $3.int_val;
        	$$ = $1;
		} |
		literal |
		function-call |
		boolean-expression;

literal: CHAR_LITERAL {
			$$ = new NChar($1.char_val);
		} | STRING_LITERAL {
			$$ = new NString($1.string_val);
		};

ident : IDENTIFIER { $$ = new NIdentifier($1.name); }
	  ;

%%
