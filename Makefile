all: parser

run: parser z.c w.c
	# ./main z.c
	./parser w.c

OBJS = parser.o  \
	   codegen.o \
	   scanner.o \
	   main.o \
	   corefn.o \

LLVMCONFIG = llvm-config
CPPFLAGS = `$(LLVMCONFIG) --cppflags` -std=c++14 -g
LDFLAGS = `$(LLVMCONFIG) --ldflags` -lpthread -ldl -lz -lncurses -rdynamic -lfl
LIBS = `$(LLVMCONFIG) --libs`

main: scanner.cpp parser.cpp parser.hpp type.h
	g++ --std=c++14 -g scanner.cpp parser.cpp -lfl -o main

scanner.cpp: scanner.l type.h
	flex -o scanner.cpp scanner.l

parser.cpp: parser.y type.h
	bison -d -o $@ $<

parser.hpp: parser.cpp

%.o: %.cpp
	g++ -c $(CPPFLAGS) -o $@ $<

parser: $(OBJS) codegen.h
	g++ -o $@ $(OBJS) $(LIBS) $(LDFLAGS)

clean:
	$(RM) -rf parser.cpp parser.hpp parser scanner.cpp $(OBJS)

