%{
	#include "type.h"
	# include "parser.hpp"
	extern YYSTYPE yylval;
	#define TOKEN(t)    (yylval.token = t)
%}


%option nodefault yylineno

comment				 	"//".*$
multiline-comment		"/*"[^"*/"]*"*/"

boolean-operator		"!"
assignment-operator		"="

identifier				[a-zA-Z_]([a-zA-Z_0-9])*

number					("-"|"+")?([0-9]*"."?[0-9]+)
integer					("-"|"+")?[0-9]+

symbol					"|"|" "|"-"|"!"|"#"|"$"|"%"|"&"|"("|")"|"*"|"+"|","|"-"|"."|"/"|":"|";"|">"|"="|"<"|"?"|"@"|"["|"\\"|"]"|"^"|"_"|"`"|"{"|"}"|"~"
char					[a-zA-Z0-9]|{symbol}
string					{char}*
char-literal			"'"{char}"'"
string-literal			"\""{string}"\""

include					"#"[ \t]*"include"


%%

[ \t\n]					{ /* ignorar espaços em branco */ }

{comment}				{ /* ignorar comentários */ }
{multiline-comment}		{ /* ignorar comentários */ }

void					{ yylval.type = VOID_TYPE; return VOID; }
char					{ yylval.type = CHAR_TYPE; return CHAR; }
int						{ yylval.type = INT_TYPE; return INT; }
double					{ yylval.type = DOUBLE_TYPE; return DOUBLE; }
string					{ yylval.type = STRING_TYPE; return STRING; }
bool					{ yylval.type = BOOL_TYPE; return BOOL; }

function				{ return FUNCTION; }
while					{ return WHILE; }
if						{ return IF; }
else					{ return ELSE; }
return					{ return RETURN; }

"+"						{ return TOKEN(PLUS); }
"-"						{ return TOKEN(MINUS); }
"*"						{ return TOKEN(TIMES); }
"/"						{ return TOKEN(DIVIDE); }

"<"           { return TOKEN(CMP_LT); }
">"           { return TOKEN(CMP_GT); }
"<="          { return TOKEN(CMP_LE); }
">="          { return TOKEN(CMP_GE); }
"=="          { return TOKEN(CMP_EQ); }
"!="          { return TOKEN(CMP_NE); }

"true"				{  return TOKEN(TRUE_T); }
"false"				{  return TOKEN(FALSE_T); }

and						{ return TOKEN(AND); }
or 						{ return TOKEN(OR); }

{boolean-operator}		{ return BOOLEAN_OPERATOR; }
{assignment-operator}	{ return ASSIGNMENT_OPERATOR; }

{identifier}			{ yylval.type_value.name = strdup(yytext); return IDENTIFIER; }

{integer}				{ yylval.type_value.type = INT_TYPE;
													yylval.type_value.set_value(atoi(yytext));
													return INTEGER; }

{number}				{ yylval.type_value.type = DOUBLE_TYPE;
													yylval.type_value.set_value(atof(yytext));
													return NUMBER; }

{char-literal}			{ yylval.type_value.type = CHAR_TYPE;
													yylval.type_value.set_value(strdup(yytext)[1]);
													return CHAR_LITERAL; }

{string-literal} {
		yylval.type_value.type = STRING_TYPE;
		yylval.type_value.set_value(std::string(yytext).substr(1, yyleng-2));
		return STRING_LITERAL;
}

{include}				{ return INCLUDE; }

"("						{ return OPEN_PARENTHESIS; }
")"						{ return CLOSE_PARENTHESIS; }
"{"						{ return OPEN_CURLY_BRACES; }
"}"						{ return CLOSE_CURLY_BRACES; }
"["						{ return OPEN_BRACKET; }
"]"						{ return CLOSE_BRACKET; }
";"						{ return SEMICOLON; }
","						{ return COMMA; }


.							{ printf("	Erro Léxico: %s\n", yytext); }


%%
