#include "type.h"
#include "codegen.h"
#include "parser.hpp"
#include "llvm/Support/TargetRegistry.h"

using namespace std;

/* Compile the AST into a module */
void CodeGenContext::generateCode(NBlock& root)
{
	std::cout << "Generating code...\n";
	
	/* Create the top level interpreter function to call as entry */
	vector<Type*> argTypes;
	FunctionType *ftype = FunctionType::get(Type::getVoidTy(MyContext), makeArrayRef(argTypes), false);
	mainFunction = Function::Create(ftype, GlobalValue::ExternalLinkage, "main", module);
	BasicBlock *bblock = BasicBlock::Create(MyContext, "entry", mainFunction, 0);
	
	/* Push a new variable/block context */
	pushBlock(bblock);
	root.codeGen(*this); /* emit bytecode for the toplevel block */
	// ReturnInst::Create(MyContext, bblock);
	ReturnInst::Create(MyContext, currentBlock());
	popAll();
	
	/* Print the bytecode in a human-readable format 
	   to see if our program compiled properly
	 */
	std::cout << "Code is generated.\n";
	// module->dump();

	legacy::PassManager pm;
	pm.add(createPrintModulePass(outs()));

	auto TargetTriple = sys::getDefaultTargetTriple();
	InitializeAllTargetInfos();
	InitializeAllTargets();
	InitializeAllTargetMCs();
	InitializeAllAsmParsers();
	InitializeAllAsmPrinters();
	std::string Error;
	auto Target = TargetRegistry::lookupTarget(TargetTriple, Error);

	// Print an error and exit if we couldn't find the requested target.
	// This generally occurs if we've forgotten to initialise the
	// TargetRegistry or we have a bogus target triple.
	if (!Target) {
		errs() << Error;
		return;
	}
	auto CPU = "generic";
	auto Features = "";

	TargetOptions opt;
	auto TargetMachine = Target->createTargetMachine(TargetTriple, CPU, Features, opt);
	module->setDataLayout(TargetMachine->createDataLayout());
	module->setTargetTriple(TargetTriple);
	auto Filename = "output.o";
	std::error_code EC;
	raw_fd_ostream dest(Filename, EC, sys::fs::F_None);

	if (EC) {
		errs() << "Could not open file: " << EC.message();
		return;
	}
	auto FileType = TargetMachine::CGFT_ObjectFile;

	if (TargetMachine->addPassesToEmitFile(pm, dest, FileType)) {
		errs() << "TargetMachine can't emit a file of this type";
		return;
	}

	pm.run(*module);
	dest.flush();

	std::cout << "Output object file generated: output.o" << std::endl;
	std::cout << "Run a linker to get an executable" << std::endl;
}

/* Executes the AST by running the main function */
GenericValue CodeGenContext::runCode() {
	std::cout << "Running code...\n";
	ExecutionEngine *ee = EngineBuilder( unique_ptr<Module>(module) ).create();
	ee->finalizeObject();
	vector<GenericValue> noargs;
	GenericValue v = ee->runFunction(mainFunction, noargs);
	std::cout << "Code was run.\n";
	return v;
}

/* Returns an LLVM type based on the identifier */
// static Type *typeOf(const NIdentifier& type) 
// {
// 	if (type.name.compare("int") == 0) {
// 		return Type::getInt64Ty(MyContext);
// 	}
// 	else if (type.name.compare("double") == 0) {
// 		return Type::getDoubleTy(MyContext);
// 	}
// 	return Type::getVoidTy(MyContext);
// }

static Type *typeOf(const var_type& type) 
{
	// VOID_TYPE, INT_TYPE, DOUBLE_TYPE, STRING_TYPE, CHAR_TYPE, BOOL_TYPE
	switch (type) {
		case INT_TYPE: return Type::getInt64Ty(MyContext);
		case DOUBLE_TYPE: return Type::getDoubleTy(MyContext);
		case BOOL_TYPE: return Type::getInt1Ty(MyContext);
		case CHAR_TYPE: return Type::getInt8Ty(MyContext);
		default: return Type::getVoidTy(MyContext);
	}
}

/* -- Code Generation -- */

Value* NInteger::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating integer: " << value << endl;
	return ConstantInt::get(Type::getInt64Ty(MyContext), value, true);
}

Value* NChar::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating char: " << value << endl;
	return ConstantInt::get(Type::getInt8Ty(MyContext), value, true);
}

Value* NDouble::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating double: " << value << endl;
	return ConstantFP::get(Type::getDoubleTy(MyContext), value);
}

Value* NString::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating string: " << value << endl;
	return ConstantDataArray::getString(MyContext, value);
	// return ConstantDataArray::get(Type::getArrayTy(MyContext), value);
}

Value* NVoid::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating void" << endl;
	return NULL;
}

Value* NBoolean::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating boolean: " << value << endl;
	return ConstantInt::get(Type::getInt1Ty(MyContext), value);
}

Value* NIdentifier::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating identifier reference: " << name << endl;
	if (context.getVariable(name) == nullptr) {
		std::cerr << "undeclared variable " << name << endl;
		return NULL;
	}

	Instruction *i = new LoadInst(context.getVariable(name), "", false, context.currentBlock());
	
	if (VectorType::classof(i->getType()))
		i = ExtractElementInst::Create(i, ConstantInt::get(Type::getInt64Ty(MyContext), index), "", context.currentBlock());

	return i;
}

Value* NFunctionCall::codeGen(CodeGenContext& context) const
{
	Function *function = context.module->getFunction(id.name.c_str());
	if (function == NULL) {
		std::cerr << "no such function " << id.name << endl;
	}
	std::vector<Value*> args;
	ExpressionList::const_iterator it;
	for (it = arguments.begin(); it != arguments.end(); it++) {
		args.push_back((**it).codeGen(context));
	}
	CallInst *call = CallInst::Create(function, makeArrayRef(args), "", context.currentBlock());
	std::cout << "Creating method call: " << id.name << endl;
	return call;
}

Value* NBinaryOperator::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating binary operation " << op << endl;
	Instruction::BinaryOps instr;
	CmpInst::Predicate pred;
	switch (op) {
		case PLUS:		instr = Instruction::Add; goto math;
		case MINUS: 	instr = Instruction::Sub; goto math;
		case TIMES: 	instr = Instruction::Mul; goto math;
		case DIVIDE: 	instr = Instruction::SDiv; goto math;
		case CMP_LT: 	pred = CmpInst::Predicate::ICMP_SLT; goto comp;
		case CMP_GT: 	pred = CmpInst::Predicate::ICMP_SGT; goto comp;
		case CMP_LE: 	pred = CmpInst::Predicate::ICMP_SLE; goto comp;
		case CMP_GE: 	pred = CmpInst::Predicate::ICMP_SGE; goto comp;
		case CMP_EQ: 	pred = CmpInst::Predicate::ICMP_EQ; goto comp;
		case CMP_NE: 	pred = CmpInst::Predicate::ICMP_NE; goto comp;
		// case OR: 	pred = CmpInst::Predicate::ICMP_NE; goto comp;
		// case AND: 	pred = CmpInst::Predicate::ICMP_NE; goto comp;
				
		/* TODO comparison */
	}

	return NULL;
math:
	return BinaryOperator::Create(instr, lhs.codeGen(context), 
		rhs.codeGen(context), "", context.currentBlock());

comp:
	return new ICmpInst(*context.currentBlock(), pred, lhs.codeGen(context), rhs.codeGen(context));
}

Value* NBlock::codeGen(CodeGenContext& context) const
{
	StatementList::const_iterator it;
	Value *last = NULL;
	for (it = statements.begin(); it != statements.end(); it++) {
		std::cout << "Generating code for " << typeid(**it).name() << endl;
		last = (**it).codeGen(context);
	}
	std::cout << "Creating block" << endl;
	return last;
}

Value* NExpressionStatement::codeGen(CodeGenContext& context) const
{
	std::cout << "Generating code for " << typeid(expression).name() << endl;
	return expression.codeGen(context);
}

Value* NReturnStatement::codeGen(CodeGenContext& context) const
{
	std::cout << "Generating return code for " << typeid(expression).name() << endl;
	Value *returnValue = expression.codeGen(context);
	context.setCurrentReturnValue(returnValue);
	return returnValue;
}

Value* NAssignment::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating assignment for " << lhs.name << endl;
	if (context.getVariable(lhs.name) == nullptr) {
		std::cerr << "undeclared variable " << lhs.name << endl;
		return NULL;
	}
	
	// If it is an array
	if (index >= 0) {
		Instruction *i = new LoadInst(context.getVariable(lhs.name), "", false, context.currentBlock());
		Value *modifiedArray = InsertElementInst::Create(i, rhs.codeGen(context), ConstantInt::get(Type::getInt64Ty(MyContext), index), "", context.currentBlock());
		return new StoreInst(modifiedArray, context.getVariable(lhs.name), false, context.currentBlock());
	}
	else {
		return new StoreInst(rhs.codeGen(context), context.getVariable(lhs.name), false, context.currentBlock());
	}
}

Value* NVariableDeclaration::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating variable declaration " << type << " " << id.name << endl;
	AllocaInst *alloc = new AllocaInst(typeOf(type), 0, id.name.c_str(), context.currentBlock());
	context.locals()[id.name] = alloc;
	if (assignmentExpr != NULL) {
		NAssignment assn(id, *assignmentExpr);
		assn.codeGen(context);
	}
	return alloc;
}

Value* NArrayDeclaration::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating array declaration " << type << " " << id.name << endl;

	AllocaInst *alloc = new AllocaInst(VectorType::get(typeOf(type), size), 0, id.name.c_str(), context.currentBlock());
	context.locals()[id.name] = alloc;

	if (values.empty()) return alloc;
	
	std::vector<Constant *> v;
	for (NExpression *e : values)
    	v.push_back((Constant *) e->codeGen(context));

	ArrayRef<Constant *> arrayRef(v);
	ConstantVector *array = (ConstantVector *) ConstantVector::get(arrayRef);

	new StoreInst(array, context.locals()[id.name], false, context.currentBlock());

	return alloc;
}

// Value* NExternDeclaration::codeGen(CodeGenContext& context) const
// {
//     vector<Type*> argTypes;
//     VariableList::const_iterator it;
//     for (it = arguments.begin(); it != arguments.end(); it++) {
//         argTypes.push_back(typeOf((**it).type));
//     }
//     FunctionType *ftype = FunctionType::get(typeOf(type), makeArrayRef(argTypes), false);
//     Function *function = Function::Create(ftype, GlobalValue::ExternalLinkage, id.name.c_str(), context.module);
//     return function;
// }

Value* NFunctionDeclaration::codeGen(CodeGenContext& context) const
{
	vector<Type*> argTypes;
	VariableList::const_iterator it;
	for (it = arguments.begin(); it != arguments.end(); it++) {
		argTypes.push_back(typeOf((**it).type));
	}
	FunctionType *ftype = FunctionType::get(typeOf(type), makeArrayRef(argTypes), false);
	Function *function = Function::Create(ftype, GlobalValue::InternalLinkage, id.name.c_str(), context.module);
	BasicBlock *bblock = BasicBlock::Create(MyContext, "entry", function, 0);

	context.pushBlock(bblock);

	Function::arg_iterator argsValues = function->arg_begin();
    Value* argumentValue;

	for (it = arguments.begin(); it != arguments.end(); it++) {
		(**it).codeGen(context);
		
		argumentValue = &*argsValues++;
		argumentValue->setName((*it)->id.name.c_str());
		StoreInst *inst = new StoreInst(argumentValue, context.locals()[(*it)->id.name], false, bblock);
	}
	
	block.codeGen(context);
	return_expr.codeGen(context);
	ReturnInst::Create(MyContext, context.getCurrentReturnValue(), context.currentBlock());

	while (context.currentBlock() != bblock) {
		context.popBlock();
	}

	context.popBlock();
	std::cout << "Creating function: " << id.name << endl;
	return function;
}

Value* NUnaryLOperator::codeGen(CodeGenContext& context) const
{
	return NULL;
}

Value* NIfStatement::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating if" << endl;

	Value *condValue = condition.codeGen(context);
	// condValue = new FCmpInst(
	// 	*context.currentBlock(),
	// 	CmpInst::FCMP_ONE,
	// 	condValue,
 // 		ConstantInt::get(getGlobalContext(), 0)
 // 	);


	Function *function = context.currentBlock()->getParent();

	BasicBlock *thenBlock = BasicBlock::Create(getGlobalContext(), "if.then", function);
	BasicBlock *elseBlock = BasicBlock::Create(getGlobalContext(), "if.else");
	BasicBlock *mergeBlock = BasicBlock::Create(getGlobalContext(), "if.cont");

	BranchInst::Create(thenBlock, elseBlock, condValue, context.currentBlock());

	context.pushBlock(thenBlock);

	Value *thenValue = trueBlock.codeGen(context);

	if (thenValue == nullptr) return nullptr;

	BranchInst::Create(mergeBlock, context.currentBlock());

	context.popBlock();




	function->getBasicBlockList().push_back(elseBlock);

	context.pushBlock(elseBlock);


	Value *elseValue = falseBlock.codeGen(context);

	// if (elseValue == nullptr) return nullptr;

	BranchInst::Create(mergeBlock, context.currentBlock());


	context.popBlock();

	function->getBasicBlockList().push_back(mergeBlock);

	context.pushBlock(mergeBlock);

	return mergeBlock;
}

Value* NWhileStatement::codeGen(CodeGenContext& context) const
{
	std::cout << "Creating if" << endl;


	Function *function = context.currentBlock()->getParent();

	BasicBlock *conditionBlock = BasicBlock::Create(getGlobalContext(), "while.condition", function);
	BasicBlock *bodyBlock = BasicBlock::Create(getGlobalContext(), "while.body");
	BasicBlock *mergeBlock = BasicBlock::Create(getGlobalContext(), "while.cont");
	
	BranchInst::Create(conditionBlock, context.currentBlock());


	function->getBasicBlockList().push_back(conditionBlock);
	context.pushBlock(conditionBlock);
	Value *condValue = condition.codeGen(context);
	BranchInst::Create(bodyBlock, mergeBlock, condValue, context.currentBlock());
	context.popBlock();

	function->getBasicBlockList().push_back(bodyBlock);
	context.pushBlock(bodyBlock);
	block.codeGen(context);
	BranchInst::Create(conditionBlock, context.currentBlock());
	context.popBlock();

	function->getBasicBlockList().push_back(mergeBlock);
	context.pushBlock(mergeBlock);
	
	return mergeBlock;
}
