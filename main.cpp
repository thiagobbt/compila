#include <iostream>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "type.h"
#include "codegen.h"

extern int yyparse();
extern NBlock* programBlock;

void createCoreFunctions(CodeGenContext& context);

int main(int argc, char **argv) {
	extern FILE *yyin;

	if(argc > 1) {
		if(!(yyin = fopen(argv[1], "r"))) {
			perror(argv[1]);
			return 1;
		}
	}
	printf("%d\n", yyparse());

	std::cout << *programBlock << std::endl;

	std::cout << "Code generation" << std::endl;

	InitializeNativeTarget();
	InitializeNativeTargetAsmPrinter();
	InitializeNativeTargetAsmParser();
	CodeGenContext context;
	createCoreFunctions(context);
	context.generateCode(*programBlock);
	context.runCode();

	return 0;
}

void yyerror(const char *s, ...) {
	const int size = strlen(s) + 1;
		char enError[size];
		memset(enError, 0, (size * sizeof(char)));
		strcpy(enError, s);
		char ptError[2*size];
		memset(ptError, 0, (2 * size * sizeof(char)));

		char *token = strtok(enError, " ,");
		while (token != NULL) {
				//printf("Token atual = %s\n", token);
				if (strcmp(token, "syntax") == 0);
				else if (strcmp(token, "error") == 0)
						strcat(ptError, "Erro Sintático, ");
				else if (strcmp(token, "unexpected") == 0)
						strcat(ptError, "símbolo incorreto: ");
				else if (strcmp(token, "expecting") == 0)
						strcat(ptError, ", símbolo esperado: ");
				else
						strcat(ptError, token);
				token = strtok(NULL, " ,");
		}
		s = ptError;

	va_list ap;
	va_start(ap, s);

		extern int yylineno;
	fprintf(stderr, "Linha %d => ", yylineno);
	vfprintf(stderr, s, ap);
	fprintf(stderr, "\n");
}